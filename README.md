[English](./README_EN.md) | 简体中文

## 交互式SICP

享受在GNU TeXmacs中阅读SICP！

在阅读的同时，可直接在GNU TeXmacs中运行Scheme代码片段。

### 阅读提示
如果您使用墨干阅读本书，请下载[墨干](https://gitee.com/XmacsLabs/mogan)之后，通过`文件->打开`打开sicp-full.tm这个文档。

如果您使用PDF阅读器，请直接在下载[最新版本的PDF文档](https://gitee.com/XmacsLabs/interactive-sicp/releases/v2)，PDF文件没有交互功能。
